console.log('migration starts')
const { Op} = require("sequelize");

const originDB = require('./database/origin/models')
const destinationDB = require('./database/destination/models')
const { migrateWithoutVariant, migrateFailed, migrateVariant} = require("./helper");

const limit = process.argv[2]
const state = process.argv[3]

async function run() {
  const t0 = new Date()
  let failIds = []
  try {
    const include = {
      model: originDB.Product,
      as: 'product',
      where: {
        supplier_id: { [Op.not]: null },
        toko_id: { [Op.is]: null },
        status: { [Op.not]: 'deleted' },
        is_tokotalk: {
          [Op.or]: [false, {[Op.is]: null}]
        }
      },
      attributes: []
    }
    let lastSuccess;
    let lastId
    let where
    let totalProduct
    let chunkSize

    if (state === 'non-variant') {
      lastSuccess = await destinationDB.MigrationLog.findOne({
        where: {success: true, type: 'single'},
        order: [['product_id', 'desc']]
      });
      lastId = lastSuccess?.last_id;
      where = {variant: {[Op.is]: null}}
      if (lastId) where.id = {[Op.gt]: lastId}

      totalProduct = await originDB.ProductImage.count({where, include});
      chunkSize = Math.ceil(totalProduct / limit);

      console.log('Total Product Images:', totalProduct)
      console.log('Total Chunk:', chunkSize)

      // migrate without variant
      await migrateWithoutVariant({lastId, chunkSize, limit, include})

      console.log('Done Migration From All Origin Database Without Variant \n\n')
    } else if (state === 'variant') {
      console.log('Start Migrating With Variant')
      const variantData = await destinationDB.OptionValue.findAll({attributes: ['id', 'name']});
      lastSuccess = await destinationDB.MigrationLog.findOne({
        where: {success: true, type: 'variant'},
        order: [['product_id', 'desc']]
      });
      lastId = lastSuccess?.last_id;

      where = {variant: {[Op.not]: null}}
      if (lastId) where.id = {[Op.gt]: lastId}

      totalProduct = await originDB.ProductImage.count({where, include});
      chunkSize = Math.ceil(totalProduct / limit);

      console.log('Total Product Images:', totalProduct)
      console.log('Total Chunk:', chunkSize)

      // migrate variant data
      await migrateVariant({variantData, chunkSize, limit, lastId, include})
      console.log('Done Migration From All Origin Database With Variant \n\n')
    } else if (state === 'retry-failed') {
      console.log('Start Migrating Failed Data')
      const variantData = await destinationDB.OptionValue.findAll({attributes: ['id', 'name']});
      totalProduct = await destinationDB.MigrationLog.count({where: {success: false}, distinct: true, col: 'last_id'})
      chunkSize = Math.ceil(totalProduct / limit)

      console.log('Total Product Images:', totalProduct)
      console.log('Total Chunk:', chunkSize)

      // migrate failed data
      let [failIdsFailed] = await migrateFailed({ chunkSize, limit, variantData})
      failIds = failIdsFailed

      if (failIds && failIds.length > 0) {
        await destinationDB.MigrationLog.bulkCreate(failIds)
      }
    } else {
      console.log(`Failed to start migration. Only variant, non-variant and retry-failed can be used for fourth statement`)
      return false;
    }

    console.log(`All migration done with elapsed time: ${new Date() - t0}`)
    return true
  }
  catch(err) {
    if (failIds.length > 0) {
      await destinationDB.MigrationLog.bulkCreate(failIds)
    }
    console.log(`migration error with elapsed time: ${new Date() - t0} and error:`)
    console.log(err)
    throw err
  }
}

run()
  .then(() => process.exit(0))
  .catch((err) => {
    console.log(err)
    process.exit(0)
  })
