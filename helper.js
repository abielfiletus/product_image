const {Op, Sequelize} = require("sequelize");
const {Console} = require("console");
const originDB = require("./database/origin/models");
const StorageSequenceSchema = require("./database/storage/sequence");
const destinationDB = require("./database/destination/models");
const StorageSchema = require("./database/storage/model");
const fs = require("fs");
const path = require("path");

const moment = require("moment-timezone");
const axios = require("axios");
const aws = require("./aws");

const ExecDownloadLogger = new Console({
  stdout: fs.createWriteStream(path.join(__dirname, 'image_download_exec_time.log'), { flags: 'a' }),
})
const ExecInsertLogger = new Console({
  stdout: fs.createWriteStream(path.join(__dirname, 'batch_insert_exec_time.log'), { flags: 'a' }),
})
const ExecUploadLogger = new Console({
  stdout: fs.createWriteStream(path.join(__dirname, 'image_upload_exec_time.log'), { flags: 'a' }),
})

const nameCleaner = name => name.toLowerCase().replace(/[^a-z0-9 ]/g, "")
const titleCase = str => str.replace(/\b\S/g, t => t.toUpperCase());

async function runMigrate(data, seq, variantData = []) {
  let mockInsert = []
  let mockStorage = []
  let filenames = []
  const successIds = []
  const failIds = []
  const variantDataLength = variantData.length

  await Promise.all(data.map(async (item, key) => {
    return await new Promise(async (resolve) => {
      item = item.toJSON()
      try {
        const date = moment(moment.now()).format('YYYY-MM-DD HH:mm:ss.SSSSSS')
        let tLoop0 = new Date()
        const splitUrl = item.url.split('.')
        const download = await axios({
          url: item.url,
          method: 'GET',
          timeout: 30000,
          responseType: 'arraybuffer',
          headers: { 'User-Agent': 'PostmanRuntime/7.26.8' }
        })
        ExecDownloadLogger.log(`Id ${item.id} and Product id ${item.toko_product_id} elapse time ${new Date() - tLoop0} ms :: ${new Date()}`)

        tLoop0 = new Date()
        const upload = await aws.upload(download.data, Date.now() + '.' + splitUrl[splitUrl.length - 1])
        ExecUploadLogger.log(`Id ${item.id} and Product id ${item.toko_product_id} elapse time ${new Date() - tLoop0} ms :: ${new Date()}`)

        let option_value_id;
        if (item.variant) {
          for (let i = 0; i < variantDataLength; i++) {
            if (titleCase(nameCleaner(item.variant)) === variantData[i].name) {
              option_value_id = variantData[i].id
              break;
            }
          }
        }

        const image_id = seq + key
        mockInsert.push({
          product_id: item.toko_product_id,
          image_id: image_id,
          option_value_id: option_value_id,
          priority: item.priority
        })
        mockStorage.push({
          insertOne: {
            document: {
              id: image_id,
              createAt: date,
              updateAt: date,
              service: 'product',
              url: upload.Location,
              filename: upload.Key,
              bucket: upload.Bucket
            }
          }
        })
        successIds.push({ product_id: item.id, last_id: item.id, type: variantDataLength ? 'variant' : 'single' })
        filenames.push(upload.Key)
        resolve()
      } catch (err) {
        failIds.push({ product_id: item.id, last_id: item.id, success: false, type: variantDataLength ? 'variant' : 'single', url: item.url })
        console.log(err)
        resolve()
      }
    })
  }))

  return { mockInsert, mockStorage, filenames, successIds, failIds }
}

exports.migrateWithoutVariant = async ({ lastId, chunkSize, limit, include }) => {
  try {
    for (let i = 0; i < chunkSize; i++) {
      const where = { variant: { [Op.is]: null } }
      let tChunk0 = new Date()
      if (lastId) where.id = { [Op.gt]: lastId }

      const [data, sequence] = await Promise.all([
        await originDB.ProductImage.findAll({ where, include, limit: limit, order: [['id', 'asc']] }),
        await StorageSequenceSchema.findOne({ name: 'storage' })
      ])

      const { mockInsert, mockStorage, filenames, successIds, failIds } = await runMigrate(data, sequence.auto.seq + 1)

      const sequelizeTrx = await destinationDB.sequelize.transaction()
      await new Promise(async (resolve) => {
        try {
          await StorageSchema.bulkWrite(mockStorage)
          await Promise.all([
            new Promise(async (resolve, reject) => {
              try {
                let tLoop0 = new Date()
                await destinationDB.ProductImage.bulkCreate(mockInsert, { transaction: sequelizeTrx })
                ExecInsertLogger.log(`Batch ${i + 1} elapse time ${new Date() - tLoop0} ms :: ${new Date().toISOString()}`)
                resolve()
              } catch (err) {
                reject(err)
              }
            }),
            await destinationDB.MigrationLog.bulkCreate(successIds, { transaction: sequelizeTrx }),
            await destinationDB.MigrationLog.bulkCreate(failIds, { transaction: sequelizeTrx }),
            await StorageSequenceSchema.updateOne({ name: 'storage' }, { auto: { field_names: ['id'], seq: sequence.auto.seq + data.length } })
          ])
          await sequelizeTrx.commit()
          resolve()
        } catch (err) {
          await sequelizeTrx.rollback()
          console.log(err)
          aws.destroy(filenames).then(() => 'Success Destroy Files')
          resolve()
        }
      })

      lastId = data.length ? data[data.length - 1].id : 0
      console.log(`Migration ${data[0].id} - ${lastId} elapsed time: ${new Date() - tChunk0} ms`)
    }

    return true
  } catch (err) {
    return true
  }
}

exports.migrateFailed = async ({ chunkSize, limit, variantData }) => {
  let failIds = []
  let lastId
  try {
    for (let i = 0; i < chunkSize; i++) {
      const ids = []
      let tChunk0 = new Date()

      const migration = await destinationDB.MigrationLog.findAll({
        attributes: [Sequelize.fn('DISTINCT', Sequelize.col('last_id')), 'last_id', 'type'],
        where: { success: false },
        limit: limit,
        order: [['last_id', 'asc']]
      })

      migration.map(item => ids.push(item.last_id))

      const [data, sequence] = await Promise.all([
        await originDB.ProductImage.findAll({ where: { id: ids } }),
        await StorageSequenceSchema.findOne({ name: 'storage' })
      ])

      const { mockInsert, mockStorage, filenames, successIds } = await runMigrate(data, sequence.auto.seq + 1, variantData)
      const sequelizeTrx = await destinationDB.sequelize.transaction()
      await new Promise(async (resolve) => {
        try {
          await StorageSchema.bulkWrite(mockStorage)
          await Promise.all([
            new Promise(async (resolve, reject) => {
              try {
                let tLoop0 = new Date()
                await destinationDB.ProductImage.bulkCreate(mockInsert, { transaction: sequelizeTrx })
                ExecInsertLogger.log(`Batch ${i + 1} elapse time ${new Date() - tLoop0} ms :: ${new Date()}`)
                resolve()
              } catch (err) {
                reject(err)
              }
            }),
            new Promise(async (resolve, reject) => {
              try {
                await Promise.all(successIds.map(async (item) => {
                  await destinationDB.MigrationLog.update({ success: true }, { where: { last_id: item.last_id } })
                }))
                resolve()
              } catch (err) {
                console.log(err)
                reject(err)
              }
            }),
            await StorageSequenceSchema.updateOne({ name: 'storage' }, { auto: { field_names: ['id'], seq: sequence.auto.seq + data.length } })
          ])
          await sequelizeTrx.commit()
          resolve()
        } catch (err) {
          await sequelizeTrx.rollback()
          console.log(err)
          aws.destroy(filenames).then(() => 'Success Destroy Files')
          resolve()
        }
      })

      lastId = data.length ? data[data.length - 1].id : 0
      console.log(`Migration ${data[0].id} - ${lastId} elapsed time: ${new Date() - tChunk0} ms`)
      failIds = []
    }

    return [null, lastId]
  } catch (err) {
    return [failIds]
  }
}

exports.migrateVariant = async ({ variantData, chunkSize, lastId, limit, include }) => {
  try {
    const variantDic = []
    for (let i = 0; i < variantData.length; i++) {
      const cleanName = await titleCase(nameCleaner(variantData[i].name))
      variantDic.push({ ...variantData[i].toJSON(), name: cleanName })
    }

    for (let i = 0; i < chunkSize; i++) {
      const where = { variant: { [Op.not]: null } }
      let tChunk0 = new Date()
      if (lastId) where.id = { [Op.gt]: lastId }

      const [data, sequence] = await Promise.all([
        await originDB.ProductImage.findAll({ where, include, limit: limit, order: [['id', 'asc']] }),
        await StorageSequenceSchema.findOne({ name: 'storage' })
      ])

      const { mockInsert, mockStorage, filenames, successIds, failIds } = await runMigrate(data, sequence.auto.seq + 1, variantDic)

      const sequelizeTrx = await destinationDB.sequelize.transaction()
      await new Promise(async (resolve) => {
        try {
          await StorageSchema.bulkWrite(mockStorage)
          await Promise.all([
            new Promise(async (resolve, reject) => {
              try {
                let tLoop0 = new Date()
                await destinationDB.ProductImage.bulkCreate(mockInsert, { transaction: sequelizeTrx })
                ExecInsertLogger.log(`Batch ${i + 1} elapse time ${new Date() - tLoop0} ms :: ${new Date().toISOString()}`)
                resolve()
              } catch (err) {
                reject(err)
              }
            }),
            await destinationDB.MigrationLog.bulkCreate(successIds, { transaction: sequelizeTrx }),
            await destinationDB.MigrationLog.bulkCreate(failIds, { transaction: sequelizeTrx }),
            await StorageSequenceSchema.updateOne({ name: 'storage' }, { auto: { field_names: ['id'], seq: sequence.auto.seq + data.length } })
          ])
          await sequelizeTrx.commit()
          resolve()
        } catch (err) {
          await sequelizeTrx.rollback()
          console.log(err)
          aws.destroy(filenames).then(() => 'Success Destroy Files')
          resolve()
        }
      })

      lastId = data.length ? data[data.length - 1].id : 0
      console.log(`Migration ${data[0].id} - ${lastId} elapsed time: ${new Date() - tChunk0} ms`)
    }

    return true
  } catch (err) {
    console.log(err)
    return true
  }
}