'use strict';
module.exports = (sequelize, DataTypes) => {
  const TokoRekening = sequelize.define('TokoRekening', {
    user_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    toko_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    bank_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    holder_name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    rekening_number: {
      allowNull: false,
      type: DataTypes.STRING
    },
    default: {
      type: DataTypes.BOOLEAN
    },
    is_validated: {
      type: DataTypes.BOOLEAN
    },
    status: {
      type: DataTypes.STRING
    }
  }, {});
  TokoRekening.associate = function(models) {
    // associations can be defined here
    TokoRekening.belongsTo(models.Bank, {
      foreignKey: 'bank_id',
      as: 'bank_details'
    });
  };
  return TokoRekening;
};