'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class VoucherUsage extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      VoucherUsage.belongsTo(models.Order, {
        foreignKey: 'order_id',
        as: 'order'
      });
      VoucherUsage.belongsTo(models.Transaction, {
        foreignKey: 'transaction_id',
        as: 'transaction'
      });
    }
  };
  VoucherUsage.init({
    transaction_id: DataTypes.INTEGER,
    order_id: DataTypes.INTEGER,
    voucher_code: DataTypes.STRING,
    deduction_amount: DataTypes.INTEGER,
    deduction_type: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'VoucherUsage',
  });
  return VoucherUsage;
};