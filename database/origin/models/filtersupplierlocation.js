'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class FilterSupplierLocation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  FilterSupplierLocation.init({
    location: DataTypes.STRING,
    provinces: DataTypes.STRING,
    cities: DataTypes.STRING,
    status: {
      type: DataTypes.BOOLEAN,
      default: true
    }
  }, {
    sequelize,
    modelName: 'FilterSupplierLocation',
  });
  return FilterSupplierLocation;
};