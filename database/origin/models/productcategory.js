'use strict';
module.exports = (sequelize, DataTypes) => {
  const ProductCategory = sequelize.define('ProductCategory', {
    toko_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    priority: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    icon: {
      type: DataTypes.STRING
    }
  }, {});
  ProductCategory.associate = function(models) {
    // associations can be defined here
    ProductCategory.hasMany(models.Product, {
      foreignKey: 'category_id'
    })
  };
  return ProductCategory;
};