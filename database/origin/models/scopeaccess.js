'use strict';
module.exports = (sequelize, DataTypes) => {
  const ScopeAccess = sequelize.define('ScopeAccess', {
    parent: {
      allowNull: false,
      type: DataTypes.STRING
    },
    scope: {
      allowNull: false,
      type: DataTypes.STRING
    },
    text: {
      allowNull: false,
      type: DataTypes.STRING
    },
  }, {});
  ScopeAccess.associate = function(models) {
    // associations can be defined here
  };
  return ScopeAccess;
};