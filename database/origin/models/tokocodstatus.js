'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TokoCODStatus extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  TokoCODStatus.init({
    toko_id: DataTypes.INTEGER,
    cod_active: DataTypes.BOOLEAN,
    action_by_last: DataTypes.STRING,
    reason_last: DataTypes.STRING,
    max_cod_running: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'TokoCODStatus',
  });
  return TokoCODStatus;
};