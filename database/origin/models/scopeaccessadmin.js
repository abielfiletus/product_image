'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ScopeAccessAdmin extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  ScopeAccessAdmin.init({
    parent: DataTypes.STRING,
    scope: DataTypes.STRING,
    text: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ScopeAccessAdmin',
  });
  return ScopeAccessAdmin;
};