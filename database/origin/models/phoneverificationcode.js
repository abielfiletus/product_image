'use strict';
module.exports = (sequelize, DataTypes) => {
  const PhoneVerificationCode = sequelize.define('PhoneVerificationCode', {
    phone: {
      allowNull: false,
      type: DataTypes.STRING
    },
    code: {
      allowNull: false,
      type: DataTypes.STRING
    },
    token: {
      allowNull: false,
      type: DataTypes.STRING
    },
    type: {
      allowNull: false,
      type: DataTypes.STRING
    },
    source: {
      allowNull: false,
      type: DataTypes.STRING
    },
    expires: {
      allowNull: false,
      type: DataTypes.DATE
    },
  }, {});
  PhoneVerificationCode.associate = function(models) {
    // associations can be defined here
  };
  return PhoneVerificationCode;
};