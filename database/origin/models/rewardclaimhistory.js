'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RewardClaimHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      RewardClaimHistory.belongsTo(models.User, {
        foreignKey: 'user_id',
        as: 'user'
      })
      RewardClaimHistory.belongsTo(models.RewardJourney, {
        foreignKey: 'journey_id',
        as: 'journey'
      })
    }
  };
  RewardClaimHistory.init({
    user_id: DataTypes.INTEGER,
    journey_id: DataTypes.INTEGER,
    status: DataTypes.STRING, // [requested, approved, rejected]
    last_action_by: DataTypes.STRING,
    last_action_date: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'RewardClaimHistory',
  });
  return RewardClaimHistory;
};