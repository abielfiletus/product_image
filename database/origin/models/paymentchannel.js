'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PaymentChannel extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  PaymentChannel.init({
    name: DataTypes.STRING,
    vendor_name: DataTypes.STRING,
    fee_percent: DataTypes.DOUBLE,
    fee_flat: DataTypes.INTEGER,
    status: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'PaymentChannel',
  });
  return PaymentChannel;
};