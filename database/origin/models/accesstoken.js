'use strict';
module.exports = (sequelize, DataTypes) => {
  const AccessToken = sequelize.define('AccessToken', {
    user_id: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    access_token: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    expires: {
      allowNull: false,
      type: DataTypes.DATE
    },
  }, {});
  AccessToken.associate = function(models) {
    // associations can be defined here
  };
  return AccessToken;
};