'use strict';
module.exports = (sequelize, DataTypes) => {
  const SupplierAddress = sequelize.define('SupplierAddress', {
    supplier_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    country: {
      allowNull: false,
      type: DataTypes.STRING
    },
    province_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    city_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    district_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    postal_code: {
      allowNull: false,
      type: DataTypes.STRING
    },
    address: {
      allowNull: false,
      type: DataTypes.STRING
    },
    address2: {
      type: DataTypes.STRING
    },
  }, {});
  SupplierAddress.associate = function(models) {
    // associations can be defined here
    SupplierAddress.belongsTo(models.Province, {
      foreignKey: 'province_id',
      as: 'province_details'
    });
    SupplierAddress.belongsTo(models.City, {
      foreignKey: 'city_id',
      as: 'city_details'
    })
    SupplierAddress.belongsTo(models.SupplierTokoTalk, {
      foreignKey: 'supplier_id',
      as: 'supplier_address_details'
    })
    SupplierAddress.belongsTo(models.District, {
      foreignKey: 'district_id',
      as: 'district_details'
    });
  };
  return SupplierAddress;
};