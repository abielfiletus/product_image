'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StatusOrder extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  StatusOrder.init({
    reseller_text: DataTypes.STRING,
    customer_text: DataTypes.STRING,
    supplier_text: DataTypes.STRING,
    description: DataTypes.TEXT,
    text_color: DataTypes.STRING,
    bg_color: DataTypes.STRING,
    note: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'StatusOrder',
  });
  return StatusOrder;
};