'use strict';
module.exports = (sequelize, DataTypes) => {
  const ProductImage = sequelize.define('ProductImage', {
    toko_product_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    url: {
      allowNull: false,
      type: DataTypes.STRING
    },
    priority: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    variant: {
      // allowNull: false,
      type: DataTypes.STRING
    },
  }, {});
  ProductImage.associate = function(models) {
    // associations can be defined here
    ProductImage.belongsTo(models.Product, {
      foreignKey: 'toko_product_id',
      as: 'product'
    });
  };
  return ProductImage;
};