'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TokoActivityRecord extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      TokoActivityRecord.belongsTo(models.Product, {
        foreignKey: 'product_id',
        as: 'product'
      });
    }
  };
  TokoActivityRecord.init({
    user_id: DataTypes.INTEGER,
    activity: DataTypes.STRING,
    activity_detail: DataTypes.STRING,
    res_status: DataTypes.INTEGER,
    res_time: DataTypes.INTEGER,
    error_message: DataTypes.STRING,
    endpoint: DataTypes.STRING,
    product_id: DataTypes.INTEGER,
    session_id: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'TokoActivityRecord',
  });
  return TokoActivityRecord;
};