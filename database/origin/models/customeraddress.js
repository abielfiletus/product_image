'use strict';
module.exports = (sequelize, DataTypes) => {
  const CustomerAddress = sequelize.define('CustomerAddress', {
    toko_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    customer_name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    customer_phone: {
      allowNull: false,
      type: DataTypes.STRING
    },
    country: {
      allowNull: false,
      type: DataTypes.STRING
    },
    province_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    city_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    district_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    postal_code: {
      allowNull: false,
      type: DataTypes.STRING
    },
    address: {
      allowNull: false,
      type: DataTypes.STRING
    },
    address2: {
      type: DataTypes.STRING
    },
    additional_address: {
      type: DataTypes.TEXT
    },
    deletedAt: {
      type: DataTypes.DATE
    }
  }, {});
  CustomerAddress.associate = function(models) {
    // associations can be defined here
    CustomerAddress.belongsTo(models.Province, {
      foreignKey: 'province_id',
      as: 'province_details'
    });
    CustomerAddress.belongsTo(models.City, {
      foreignKey: 'city_id',
      as: 'city_details'
    })
    CustomerAddress.belongsTo(models.Toko, {
      foreignKey: 'toko_id',
      as: 'customer_addresses'
    })
    CustomerAddress.belongsTo(models.District, {
      foreignKey: 'district_id',
      as: 'district_details'
    });
  };
  return CustomerAddress;
};