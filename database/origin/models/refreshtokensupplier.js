'use strict';
module.exports = (sequelize, DataTypes) => {
  const RefreshTokenSupplier = sequelize.define('RefreshTokenSupplier', {
    supplier_user_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    supplier_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    refresh_token: {
      allowNull: false,
      type: DataTypes.STRING
    },
    expires: {
      allowNull: false,
      type: DataTypes.DATE
    },
    scopes: {
      allowNull: false,
      type: DataTypes.TEXT
    },
  }, {});
  RefreshTokenSupplier.associate = function(models) {
    // associations can be defined here
  };
  return RefreshTokenSupplier;
};