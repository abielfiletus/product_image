'use strict';
module.exports = (sequelize, DataTypes) => {
  const Transaction = sequelize.define('Transaction', {
    toko_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    date: {
      allowNull: false,
      type: DataTypes.DATE
    },
    expiry: {
      allowNull: false,
      type: DataTypes.DATE
    },
    status: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    session_id: {
      // allowNull: false,
      type: DataTypes.STRING
    },
    payment_id: {
      type: DataTypes.STRING
    },
    payment_url: {
      // allowNull: false,
      type: DataTypes.TEXT
    },
    payment_method: {
      // allowNull: false,
      type: DataTypes.STRING
    },
    payment_status: {
      // allowNull: false,
      type: DataTypes.STRING
    },
    amount: {
      // allowNull: false,
      type: DataTypes.INTEGER
    },
    amount_paid: {
      // allowNull: false,
      type: DataTypes.INTEGER
    },
    bank_code: {
      type: DataTypes.STRING
    },
    paid_at: {
      type: DataTypes.DATE
    },
    description: {
      type: DataTypes.STRING
    },
    currency: {
      allowNull: false,
      type: DataTypes.STRING,
      defaultValue: 'IDR'
    },
    payment_channel: {
      // allowNull: false,
      type: DataTypes.STRING
    },
    payment_destination: {
      type: DataTypes.STRING
    },
    admin_fee: {
      type: DataTypes.INTEGER
    },
    checkout_channel: {
      type: DataTypes.STRING
    },
  }, {});
  Transaction.associate = function(models) {
    // associations can be defined here
    Transaction.hasMany(models.Order, {
      foreignKey: 'transaction_id',
      as: 'orders'
    });
    Transaction.belongsTo(models.StatusOrder, {
      foreignKey: 'status',
      as: 'status_details'
    });
  };
  return Transaction;
};