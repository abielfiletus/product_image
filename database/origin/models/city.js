'use strict';
module.exports = (sequelize, DataTypes) => {
  const City = sequelize.define('City', {
    province_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    type: {
      allowNull: false,
      type: DataTypes.STRING
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    postal_code: {
      allowNull: false,
      type: DataTypes.STRING
    },
  }, {});
  City.associate = function(models) {
    // associations can be defined here
  };
  return City;
};