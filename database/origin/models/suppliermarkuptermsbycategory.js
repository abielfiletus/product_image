'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SupplierMarkupTermsByCategory extends Model {
    static associate(models) {
      // define association here
    }
  };
  SupplierMarkupTermsByCategory.init({
    category_id: {
      allowNull: false,
      type:  DataTypes.STRING,
    },
    markup: {
      allowNull: false,
      type:  DataTypes.INTEGER,
    },
  }, {
    sequelize,
    modelName: 'SupplierMarkupTermsByCategory',
  });
  return SupplierMarkupTermsByCategory;
};