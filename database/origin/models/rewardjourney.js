'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RewardJourney extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      RewardJourney.belongsTo(models.RewardPrize, {
        foreignKey: 'prize_id',
        as: 'prize'
      })
      RewardJourney.belongsTo(models.RewardCheckpoint, {
        foreignKey: 'checkpoint_id',
        as: 'checkpoint'
      })
    }
  };
  RewardJourney.init({
    prize_id: DataTypes.INTEGER,
    checkpoint_id: DataTypes.INTEGER,
    status: DataTypes.BOOLEAN // [true, false]
  }, {
    sequelize,
    modelName: 'RewardJourney',
  });
  return RewardJourney;
};