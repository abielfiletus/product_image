'use strict';
module.exports = (sequelize, DataTypes) => {
  const SupplierTokoTalk = sequelize.define('SupplierTokoTalk', {
    name: DataTypes.STRING,
    location: DataTypes.TEXT,
    priority: DataTypes.INTEGER,
    photo: DataTypes.STRING,
    link: DataTypes.STRING,
    status: DataTypes.BOOLEAN,
  }, {});
  SupplierTokoTalk.associate = function(models) {
    // associations can be defined here
    SupplierTokoTalk.hasOne(models.SupplierAddress, {
      foreignKey: 'supplier_id',
      as: 'supplier_address_details'
    });
  };
  return SupplierTokoTalk;
};