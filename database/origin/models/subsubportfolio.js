'use strict';
module.exports = (sequelize, DataTypes) => {
  const SubSubPortfolio = sequelize.define('SubSubPortfolio', {
    subportfolio_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    image: DataTypes.STRING,
    priority: DataTypes.INTEGER,
    category_1: DataTypes.STRING,
    category_2: DataTypes.STRING,
    category_3: DataTypes.STRING
  }, {});
  SubSubPortfolio.associate = function(models) {
    // associations can be defined here
    SubSubPortfolio.belongsTo(models.SubPortfolio, {
      foreignKey: 'subportfolio_id',
      as: 'subsubportfolio'
    })
  };
  return SubSubPortfolio;
};