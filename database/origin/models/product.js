'use strict';
module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    toko_id: {
      type: DataTypes.INTEGER
    },
    safe_url: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    category_id: {
      type: DataTypes.INTEGER
    },
    price: {
      // allowNull: false,
      type: DataTypes.BIGINT
    },
    weight: {
      type: DataTypes.INTEGER
    },
    description: {
      type: DataTypes.TEXT
    },
    capital_price: {
      type: DataTypes.BIGINT
    },
    sku: {
      type: DataTypes.STRING
    },
    stock: {
      type: DataTypes.INTEGER
    },
    minimum_stock: {
      type: DataTypes.INTEGER
    },
    total_view: {
      type: DataTypes.INTEGER
    },
    total_share: {
      type: DataTypes.INTEGER
    },
    status: {
      type: DataTypes.STRING,
      defaultValue: 'active',
    },
    featured: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    enable_notification: {
      type: DataTypes.STRING
    },
    is_from_supplier: {
      type: DataTypes.BOOLEAN
    },
    supplier_id: {
      type: DataTypes.INTEGER
    },
    source_url: {
      type: DataTypes.STRING
    },
    discount: {
      type: DataTypes.INTEGER
    },
    discount_type: {
      type: DataTypes.STRING
    },
    video_url: {
      type: DataTypes.STRING
    },
    categories: {
      type: DataTypes.STRING
    },
    is_tokotalk: {
      type: DataTypes.BOOLEAN
    },
    quality: {
      type: DataTypes.STRING
    },
    view_booster: {
      type: DataTypes.INTEGER
    },
    QC_passed: {
      type: DataTypes.BOOLEAN
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {});

  Product.associate = function (models) {
    // associations can be defined here
    Product.hasMany(models.ProductImage, {
      foreignKey: 'toko_product_id',
      as: 'images'
    });
    Product.hasMany(models.ProductOptions, {
      foreignKey: 'product_id',
      as: 'options'
    });
    Product.belongsTo(models.ProductCategory, {
      foreignKey: 'category_id',
      as: 'category'
    });
    // Product.belongsTo(models.SupplierTokoTalk, {
    //   foreignKey: 'supplier_id',
    //   as: 'supplier'
    // });
    Product.belongsTo(models.Toko, {
      foreignKey: 'toko_id',
      as: 'toko'
    });
    Product.hasMany(models.ProductVariants, {
      foreignKey: 'product_id',
      as: 'variants'
    });
    Product.belongsTo(models.Supplier, {
      foreignKey: 'supplier_id',
      as: 'supplier'
    });
  };
  return Product;
};