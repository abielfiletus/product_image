'use strict';
module.exports = (sequelize, DataTypes) => {
  const WithdrawHistory = sequelize.define('WithdrawHistory', {
    user_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    toko_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    total_amount: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    commission_amount: {
      type: DataTypes.INTEGER
    },
    sales_amount: {
      type: DataTypes.INTEGER
    },
    rekening_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    channel: {
      type: DataTypes.STRING
    },
    fee: {
      type: DataTypes.INTEGER
    },
    status: {
      allowNull: false,
      type: DataTypes.STRING
    },
    request_date: {
      allowNull: false,
      type: DataTypes.DATE
    },
    approved_date: {
      type: DataTypes.DATE
    },
    rejected_date: {
      type: DataTypes.DATE
    },
    rejected_reason: {
      type: DataTypes.TEXT
    },
    last_action_by: {
      type: DataTypes.STRING
    },
    ref_no: {
      type: DataTypes.STRING
    },
    internal_ref_no: {
      type: DataTypes.STRING
    },
    ready_to_release: {
      type: DataTypes.BOOLEAN
    }
  }, {});
  WithdrawHistory.associate = function(models) {
    // associations can be defined here
    // TokoAddress.belongsTo(models.Toko, {
    //   foreignKey: 'toko_id',
    //   as: 'addresses'
    // })
    WithdrawHistory.belongsTo(models.TokoRekening, {
      foreignKey: 'rekening_id',
      as: 'rekening_details'
    })
    WithdrawHistory.belongsTo(models.UserKYC, {
      foreignKey: 'user_id',
      targetKey: 'user_id',
      as: 'kyc_details'
    })
  };
  return WithdrawHistory;
};