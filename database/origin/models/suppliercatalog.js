'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SupplierCatalog extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      SupplierCatalog.belongsTo(models.Supplier, {
        foreignKey: 'supplier_id',
        as: 'supplier'
      });
      SupplierCatalog.belongsTo(models.ProductVariants, {
        foreignKey: 'product_variant_id',
        as: 'supplier_catalog_details'
      });
      SupplierCatalog.belongsTo(models.Product, {
        foreignKey: 'product_id',
        as: 'product'
      });
    }
  };
  SupplierCatalog.init({
    supplier_id:  DataTypes.INTEGER,
    product_variant_id: DataTypes.INTEGER,
    product_id: DataTypes.INTEGER,
    markup: DataTypes.INTEGER,
    minimum_reseller_markup: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    status: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
  }, {
    sequelize,
    modelName: 'SupplierCatalog',
  });
  return SupplierCatalog;
};