'use strict';
module.exports = (sequelize, DataTypes) => {
  const Toko = sequelize.define('Toko', {
    user_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    business_type_id: {
      type: DataTypes.INTEGER
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    description: {
      type: DataTypes.TEXT
    },
    icon: {
      type: DataTypes.STRING
    },
    link: {
      allowNull: false,
      type: DataTypes.STRING
    },
    whatsapp: {
      allowNull: false,
      type: DataTypes.STRING
    },
    instagram: {
      type: DataTypes.STRING
    },
    facebook: {
      type: DataTypes.STRING
    },
    line: {
      type: DataTypes.STRING
    },
    youtube: {
      type: DataTypes.STRING
    },
    tokopedia: {
      type: DataTypes.STRING
    },
    shopee: {
      type: DataTypes.STRING
    },
    blibli: {
      type: DataTypes.STRING
    },
    total_visit: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    status: {
      type: DataTypes.STRING,
      defaultValue: 'active',
    },
    is_phone_verified: {
      type: DataTypes.BOOLEAN,
    },
    is_cod_active: {
      type: DataTypes.BOOLEAN,
    },
    max_cod_running: {
      type: DataTypes.INTEGER,
      defaultValue: 10
    },
    max_cod_failed: {
      type: DataTypes.INTEGER,
      defaultValue: 5
    },
  }, {});
  Toko.associate = function(models) {
    // associations can be defined here
    Toko.belongsTo(models.BusinessType, {
      foreignKey: 'business_type_id',
      as: 'business_type'
    });
    Toko.hasMany(models.TokoAddress, {
      foreignKey: 'toko_id',
      as: 'addresses'
    });
    Toko.belongsTo(models.User, {
      foreignKey: 'user_id',
      as: 'user_details'
    });
  };
  return Toko;
};