'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductViewBooster extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ProductViewBooster.belongsTo(models.Product, {
        foreignKey: 'product_id',
        as: 'product'
      });
    }
  };
  ProductViewBooster.init({
    product_id: {
      allowNull: false,
      type: DataTypes.INTEGER,
      unique : true
    },
    view_booster: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    start_date: DataTypes.STRING,
    end_date: DataTypes.STRING,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ProductViewBooster',
  });
  return ProductViewBooster;
};