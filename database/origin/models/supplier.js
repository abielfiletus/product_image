'use strict';
module.exports = (sequelize, DataTypes) => {
  const Supplier = sequelize.define('Supplier', {
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    alias: {
      type: DataTypes.STRING
    },
    phone: {
      type: DataTypes.STRING
    },
    status: {
      type: DataTypes.STRING
    },
    code: {
      type: DataTypes.STRING
    },
    last_action_by: {
      type: DataTypes.STRING
    },
    photo: {
      type: DataTypes.STRING
    },
    bd_id: {
      type: DataTypes.INTEGER
    }
  }, {});
  Supplier.associate = function(models) {
    // associations can be defined here
    Supplier.hasMany(models.SupplierRekening, {
      foreignKey: 'supplier_id',
      as: 'rekening_list'
    });
    Supplier.hasMany(models.Product, {
      foreignKey: 'supplier_id',
      as: 'product_list'
    });
    Supplier.hasOne(models.SupplierTokoAddress, {
      foreignKey: 'supplier_id',
      as: 'address_details'
    });
    Supplier.hasMany(models.SupplierCourierService, {
      foreignKey: 'supplier_id',
      as: 'courier_services'
    });
    Supplier.hasMany(models.SupplierCourierCOD, {
      foreignKey: 'supplier_id',
      as: 'courier_cod_services'
    });
    Supplier.hasOne(models.SupplierKYC, {
      foreignKey: 'supplier_id',
      as: 'kyc_details'
    });
    Supplier.belongsTo(models.BD, {
      foreignKey: 'bd_id',
      as: 'bd'
    })
  };
  return Supplier;
};