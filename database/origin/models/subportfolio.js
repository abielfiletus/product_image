'use strict';
module.exports = (sequelize, DataTypes) => {
  const SubPortfolio = sequelize.define('SubPortfolio', {
    portfolio_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    image: DataTypes.STRING,
    priority: DataTypes.INTEGER
  }, {});
  SubPortfolio.associate = function(models) {
    // associations can be defined here
    SubPortfolio.belongsTo(models.Portfolio, {
      foreignKey: 'portfolio_id',
      as: 'subportfolio'
    });
    SubPortfolio.hasMany(models.SubSubPortfolio, {
      foreignKey: 'subportfolio_id',
      as: 'subsubportfolio'
    })
  };
  return SubPortfolio;
};