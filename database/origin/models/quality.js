'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductQuality extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  ProductQuality.init({
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ProductQuality',
  });
  return ProductQuality;
};