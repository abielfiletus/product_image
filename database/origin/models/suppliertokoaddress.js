'use strict';
module.exports = (sequelize, DataTypes) => {
  const SupplierTokoAddress = sequelize.define('SupplierTokoAddress', {
    supplier_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    country: {
      allowNull: false,
      type: DataTypes.STRING
    },
    province_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    city_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    district_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    postal_code: {
      allowNull: false,
      type: DataTypes.STRING
    },
    address: {
      allowNull: false,
      type: DataTypes.STRING
    },
    address2: {
      type: DataTypes.STRING
    },
    latitude: {
      type: DataTypes.STRING
    },
    longitude: {
      type: DataTypes.STRING
    },
  }, {});
  SupplierTokoAddress.associate = function(models) {
    // associations can be defined here
    SupplierTokoAddress.belongsTo(models.Province, {
      foreignKey: 'province_id',
      as: 'province_details'
    });
    SupplierTokoAddress.belongsTo(models.City, {
      foreignKey: 'city_id',
      as: 'city_details'
    })
    SupplierTokoAddress.belongsTo(models.District, {
      foreignKey: 'district_id',
      as: 'district_details'
    });
    SupplierTokoAddress.belongsTo(models.Supplier, {
      foreignKey: 'supplier_id',
      as: 'supplier_details'
    });
  };
  return SupplierTokoAddress;
};