'use strict';
module.exports = (sequelize, DataTypes) => {
  const Courier = sequelize.define('Courier', {
    code: {
      allowNull: false,
      type: DataTypes.STRING
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    image: {
      type: DataTypes.STRING
    },
    status: {
      allowNull: false,
      type: DataTypes.STRING
    },
    tracking_web: {
      type: DataTypes.STRING
    },
  }, {});
  Courier.associate = function(models) {
    // associations can be defined here
    Courier.hasMany(models.CourierService, {
      foreignKey: 'courier_id',
      as: 'services'
    })
  };
  return Courier;
};