'use strict';
module.exports = (sequelize, DataTypes) => {
  const Cart = sequelize.define('Cart', {
    session_id: {
      // allowNull: false,
      type: DataTypes.STRING,
    },
    toko_id: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    product_id: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    quantity: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    transaction_id: {
      type: DataTypes.INTEGER,
      defaultValue: null
    },
    sku: {
      // allowNull: false,
      type: DataTypes.STRING,
    },
    product_variant_code: {
      type: DataTypes.STRING,
    },
    status:{
      type: DataTypes.STRING,
      defaultValue: 'available'
    }
  }, {});
  Cart.associate = function (models) {
    // associations can be defined here
    Cart.belongsTo(models.Product, {
      foreignKey: 'product_id',
      as: 'product_details'
    });
    // Cart.belongsTo(models.ProductVariants, {
    //   foreignKey: 'product_variant_code',
    //   as: 'details',
    //   targetKey: 'code'
    // })
    Cart.belongsTo(models.ProductSKU, {
      foreignKey: 'product_variant_code',
      as: 'details',
      targetKey: 'code'
    })
  };
  return Cart;
};