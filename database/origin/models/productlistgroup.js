'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductListGroup extends Model {
    static associate(models) {
      // define association here
    }
  };
  ProductListGroup.init({
    display_name: DataTypes.STRING,
    code: DataTypes.STRING,
    is_active: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'ProductListGroup',
  });
  return ProductListGroup;
};