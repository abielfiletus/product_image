'use strict';
module.exports = (sequelize, DataTypes) => {
  const VerificationSupplierCode = sequelize.define('VerificationSupplierCode', {
    supplier_user_id: {
      type: DataTypes.INTEGER
    },
    token: {
      allowNull: false,
      type: DataTypes.STRING
    },
    expires: {
      allowNull: false,
      type: DataTypes.DATE
    },
    source: {
      allowNull: false,
      type: DataTypes.STRING
    },
    data: {
      type: DataTypes.STRING
    },
  }, {});
  VerificationSupplierCode.associate = function(models) {
    // associations can be defined here
  };
  return VerificationSupplierCode;
};