'use strict';
module.exports = (sequelize, DataTypes) => {
  const AppInfo = sequelize.define('AppInfo', {
    latest_version: DataTypes.STRING,
    force_update: DataTypes.BOOLEAN,
    is_maintenance: DataTypes.BOOLEAN
  }, {});
  AppInfo.associate = function(models) {
    // associations can be defined here
  };
  return AppInfo;
};