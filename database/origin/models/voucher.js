'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Voucher extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Voucher.belongsTo(models.Supplier, {
        foreignKey: 'target_supplier',
        as: 'supplier'
      })
    }
  };
  Voucher.init({
    priority: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    description: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    logo: {
      allowNull: false,
      type: DataTypes.STRING
    },
    banner: {
      allowNull: false,
      type: DataTypes.STRING
    },
    tnc: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    code: {
      allowNull: false,
      type: DataTypes.STRING
    },
    percentage: {
      type: DataTypes.INTEGER
    },
    max_amount: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    quota: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    current_quota: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    limit_per_user: {
      type: DataTypes.INTEGER
    },
    sponsored_by: {
      type: DataTypes.STRING
    },
    start_date: {
      allowNull: false,
      type: DataTypes.STRING
    },
    end_date: {
      allowNull: false,
      type: DataTypes.STRING
    },
    start_time: {
      type: DataTypes.STRING
    },
    end_time: {
      type: DataTypes.STRING
    },
    min_qty_item: {
      type: DataTypes.INTEGER
    },
    min_weight: {
      type: DataTypes.INTEGER
    },
    min_order_amount: {
      type: DataTypes.INTEGER
    },
    min_transaction_amount: {
      type: DataTypes.INTEGER
    },
    category_target: {
      type: DataTypes.ARRAY(DataTypes.STRING)
    },
    payment_method: {
      type: DataTypes.ARRAY(DataTypes.INTEGER)
    },
    platform: {
      type: DataTypes.STRING
    },
    target_courier: {
      type: DataTypes.ARRAY(DataTypes.INTEGER)
    },
    target_supplier: {
      type: DataTypes.INTEGER
    },
    target_user: {
      type: DataTypes.STRING
    },
    target_destination_city: {
      type: DataTypes.ARRAY(DataTypes.INTEGER)
    },
    deduction_type: {
      allowNull: false,
      type: DataTypes.STRING
    },
    voucher_level: {
      allowNull: false,
      type: DataTypes.STRING
    },
    show: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    tags: {
      type: DataTypes.STRING
    },
    is_active: {
      type: DataTypes.BOOLEAN
    },
    is_deleted: {
      type: DataTypes.BOOLEAN
    },
  }, {
    sequelize,
    modelName: 'Voucher',
  });
  return Voucher;
};