'use strict';
module.exports = (sequelize, DataTypes) => {
  const SupplierCourierCOD = sequelize.define('SupplierCourierCOD', {
    supplier_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    courier_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
  }, {});
  SupplierCourierCOD.associate = function(models) {
    // associations can be defined here
  };
  return SupplierCourierCOD;
};