'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RefreshTokenAdmin extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  RefreshTokenAdmin.init({
    admin_user_id: DataTypes.INTEGER,
    refresh_token: DataTypes.STRING,
    expire: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'RefreshTokenAdmin',
  });
  return RefreshTokenAdmin;
};