'use strict';

module.exports = (sequelize, DataTypes) => {
  const BD = sequelize.define('BD', {
    name: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {})
  BD.associate = function(models) {
    BD.hasMany(models.Supplier, {
      foreignKey: 'bd_id',
      as: 'bd_suppliers'
    });
  }
  return BD;
};