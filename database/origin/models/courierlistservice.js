'use strict';
module.exports = (sequelize, DataTypes) => {
  const CourierListService = sequelize.define('CourierListService', {
    courier_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    service: {
      allowNull: false,
      type: DataTypes.STRING
    },
    etd: {
      type: DataTypes.STRING
    },
    notes: {
      type: DataTypes.TEXT
    },
  }, {});
  CourierListService.associate = function(models) {
    // associations can be defined here
    CourierListService.belongsTo(models.CourierList, {
      foreignKey: 'courier_id',
      as: 'services'
    })
  };
  return CourierListService;
};