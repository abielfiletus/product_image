'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SupplierMarkupTermsByPrice extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  SupplierMarkupTermsByPrice.init({
    lowerbound: {
      allowNull: false,
      type:  DataTypes.INTEGER,
    },
    upperbound: {
      allowNull: false,
      type:  DataTypes.INTEGER,
    },
    markup: {
      allowNull: false,
      type:  DataTypes.INTEGER,
    }
  }, {
    sequelize,
    modelName: 'SupplierMarkupTermsByPrice',
  });
  return SupplierMarkupTermsByPrice;
};