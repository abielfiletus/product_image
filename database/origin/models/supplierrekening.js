'use strict';
module.exports = (sequelize, DataTypes) => {
  const SupplierRekening = sequelize.define('SupplierRekening', {
    supplier_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    bank_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    holder_name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    rekening_number: {
      allowNull: false,
      type: DataTypes.STRING
    },
    default: {
      type: DataTypes.BOOLEAN
    },
    is_validate: {
      type: DataTypes.BOOLEAN
    },
    status: {
      type: DataTypes.STRING
    },
  }, {});
  SupplierRekening.associate = function(models) {
    // associations can be defined here
    SupplierRekening.belongsTo(models.Bank, {
      foreignKey: 'bank_id',
      as: 'bank_details'
    });
  };
  return SupplierRekening;
};