'use strict';
module.exports = (sequelize, DataTypes) => {
  const Order = sequelize.define('Order', {
    transaction_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    toko_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    customer_id: {
      type: DataTypes.INTEGER
    },
    invoice_no: {
      allowNull: false,
      type: DataTypes.STRING
    },
    amount: {
      allowNull: false,
      type: DataTypes.BIGINT
    },
    amount_paid: {
      // allowNull: false,
      type: DataTypes.BIGINT
    },
    admin_fee: {
      type: DataTypes.INTEGER
    },
    customer_name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    customer_phone: {
      allowNull: false,
      type: DataTypes.STRING
    },
    customer_email: {
      allowNull: false,
      type: DataTypes.STRING
    },
    status: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    country: {
      allowNull: false,
      type: DataTypes.STRING,
      defaultValue: 'ID'
    },
    province_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    city_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    district_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    postal_code: {
      allowNull: false,
      type: DataTypes.STRING
    },
    address: {
      allowNull: false,
      type: DataTypes.STRING
    },
    address2: {
      type: DataTypes.STRING
    },
    date: {
      allowNull: false,
      type: DataTypes.DATE
    },
    shipping_method: {
      allowNull: false,
      type: DataTypes.STRING
    },
    shipping_carrier: {
      type: DataTypes.STRING
    },
    shipping_date: {
      type: DataTypes.DATE
    },
    shipping_fee: {
      type: DataTypes.INTEGER
    },
    shipping_fee_paid: {
      type: DataTypes.INTEGER
    },
    shipping_traceno: {
      type: DataTypes.STRING,
      unique: {
        args: true,
        msg: 'Permintaan gagal silahkan coba lagi'
      }
    },
    payment_channel: {
      type: DataTypes.STRING
    },
    payment_method: {
      type: DataTypes.STRING
    },
    payment_date: {
      type: DataTypes.DATE
    },
    supplier_id: {
      type: DataTypes.INTEGER
    },
    is_tokotalk: {
      type: DataTypes.BOOLEAN
    },
    total_weight: {
      type: DataTypes.INTEGER
    },
    checkout_channel: {
      type: DataTypes.STRING
    },
    shipping_fee_discount_code: {
      type: DataTypes.STRING
    },
    shipping_fee_discount_nominal: {
      type: DataTypes.INTEGER
    },
    shipping_reference_no: {
      type: DataTypes.STRING,
      unique: {
        args: true,
        msg: 'Permintaan gagal silahkan coba lagi'
      }
    },
    shipping_sla: {
      type: DataTypes.STRING
    },
  }, {});
  Order.associate = function(models) {
    // associations can be defined here
    Order.belongsTo(models.Transaction, {
      foreignKey: 'transaction_id',
      as: 'order'
    });
    Order.hasMany(models.OrderDetail, {
      foreignKey: 'order_id',
      as: 'details'
    });
    Order.belongsTo(models.Toko, {
      foreignKey: 'toko_id',
      as: 'toko'
    });
    // Order.belongsTo(models.SupplierTokoTalk, {
    //   foreignKey: 'supplier_id',
    //   as: 'supplier'
    // });
    Order.belongsTo(models.Supplier, {
      foreignKey: 'supplier_id',
      as: 'supplier'
    });
    Order.belongsTo(models.Province, {
      foreignKey: 'province_id',
      as: 'province_details'
    });
    Order.belongsTo(models.City, {
      foreignKey: 'city_id',
      as: 'city_details'
    });
    Order.belongsTo(models.District, {
      foreignKey: 'district_id',
      as: 'district_details'
    });
    Order.hasOne(models.OrderTimeline, {
      foreignKey: 'order_id',
      as: 'timeline'
    });
    Order.belongsTo(models.StatusOrder, {
      foreignKey: 'status',
      as: 'status_details'
    });
  };
  return Order;
};