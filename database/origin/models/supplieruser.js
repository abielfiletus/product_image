'use strict';
module.exports = (sequelize, DataTypes) => {
  const SupplierUser = sequelize.define('SupplierUser', {
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING
    },
    photo: {
      type: DataTypes.STRING
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING
    },
    phone: {
      type: DataTypes.STRING
    },
    role: {
      allowNull: false,
      type: DataTypes.STRING
    },
    supplier_id: {
      type: DataTypes.INTEGER
    },
    is_phone_verified: {
      type: DataTypes.BOOLEAN
    },
    status: {
      type: DataTypes.STRING
    },
  }, {});
  SupplierUser.associate = function(models) {
    // associations can be defined here
    SupplierUser.hasMany(models.SupplierUserScope, {
      foreignKey: 'supplier_user_id',
      as: 'scopes'
    });
  };
  return SupplierUser;
};