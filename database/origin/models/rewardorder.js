'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RewardOrder extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      RewardOrder.belongsTo(models.RewardTransaction, {
        foreignKey: 'reward_trx_id',
        as: 'rewardTransaction'
      }),
      RewardOrder.belongsTo(models.Order, {
        foreignKey: 'order_id',
        as: 'order'
      })
    }
  };
  RewardOrder.init({
    reward_trx_id: DataTypes.INTEGER,
    order_id: DataTypes.INTEGER,
    amount: DataTypes.INTEGER,
    rewarded: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'RewardOrder',
  });
  return RewardOrder;
};