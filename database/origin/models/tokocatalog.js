'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TokoCatalog extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      TokoCatalog.belongsTo(models.Toko, {
        foreignKey: 'toko_id',
        as: 'toko'
      });
      TokoCatalog.belongsTo(models.ProductVariants, {
        foreignKey: 'product_variant_id',
        as: 'toko_catalog'
      });
      TokoCatalog.belongsTo(models.Product, {
        foreignKey: 'product_id',
        as: 'product'
      });
      TokoCatalog.belongsTo(models.ProductCategory, {
        foreignKey: 'category_id',
        as: 'category_details'
      });
    }
  };
  TokoCatalog.init({
    toko_id: DataTypes.INTEGER,
    product_variant_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    markup: DataTypes.INTEGER,
    status: DataTypes.BOOLEAN,
    dropship: DataTypes.BOOLEAN,
    product_id: DataTypes.INTEGER,
    category_id: DataTypes.INTEGER,
    featured: DataTypes.BOOLEAN,
  }, {
    sequelize,
    modelName: 'TokoCatalog',
  });
  return TokoCatalog;
};