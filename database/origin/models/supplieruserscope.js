'use strict';
module.exports = (sequelize, DataTypes) => {
  const SupplierUserScope = sequelize.define('SupplierUserScope', {
    supplier_user_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    scope: {
      allowNull: false,
      type: DataTypes.STRING
    },
  }, {});
  SupplierUserScope.associate = function(models) {
    // associations can be defined here
    SupplierUserScope.belongsTo(models.SupplierUser, {
      foreignKey: 'supplier_user_id',
      as: 'scopes'
    });
  };
  return SupplierUserScope;
};