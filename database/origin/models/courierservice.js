'use strict';
module.exports = (sequelize, DataTypes) => {
  const CourierService = sequelize.define('CourierService', {
    courier_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    service: {
      allowNull: false,
      type: DataTypes.STRING
    },
    etd: {
      type: DataTypes.STRING
    },
    is_api_support: {
      type: DataTypes.BOOLEAN
    },
    notes: {
      type: DataTypes.TEXT
    },
  }, {});
  CourierService.associate = function(models) {
    // associations can be defined here
    CourierService.belongsTo(models.Courier, {
      foreignKey: 'courier_id',
      as: 'services'
    })
  };
  return CourierService;
};