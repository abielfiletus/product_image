const mongoose = require('./connection');
const DataType = mongoose.Schema.Types;

const SequenceSchema = new mongoose.Schema({
  auto: {
    field_names: {
      type: DataType.Array
    },
    seq: {
      type: DataType.Number
    }
  },
  name: {
    type: DataType.String
  }
})

const Sequence = mongoose.model('__schema__', SequenceSchema, '__schema__');

module.exports = Sequence