const mongoose = require('mongoose');

mongoose.connect('mongodb://adminuser:password123@mongodb-dev.selleri.id:32000/storageService?authSource=admin&readPreference=primary&directConnection=true&ssl=false', {
  useUnifiedTopology: true,
  useNewUrlParser: true,
});

// mongoose.set("debug", (collectionName, method, query, doc) => {
//   console.log(`${collectionName}.${method}`, JSON.stringify(query), doc);
// });

mongoose.connection.on('connected', () => {
  console.log('Connected to MongoDB');
});
mongoose.connection.on('error', (error) => {
  console.log(error);
});

module.exports = mongoose;