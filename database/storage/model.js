const mongoose = require('./connection');
const DataType = mongoose.Schema.Types;

const StorageSchema = new mongoose.Schema({
  id: {
    type: DataType.Number
  },
  bucket: {
    type: DataType.String
  },
  createAt: {
    type: DataType.String
  },
  filename: {
    type: DataType.String
  },
  service: {
    type: DataType.String
  },
  updateAt: {
    type: DataType.String
  },
  url: {
    type: DataType.String
  }
})

const Storage = mongoose.model('storage', StorageSchema, 'storage');

module.exports = Storage