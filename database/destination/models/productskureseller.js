'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductSkuReseller extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      ProductSkuReseller.belongsTo(models.ProductSku, {
        foreignKey: 'sku_id',
        as: 'sku_detail'
      })
    }
  };
  ProductSkuReseller.init({
    sku_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    toko_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    markup: {
      type: DataTypes.NUMERIC,
      defaultValue: 0
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {
    sequelize,
    modelName: 'ProductSkuReseller',
    paranoid: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  });
  return ProductSkuReseller;
};