'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductSku extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      ProductSku.belongsTo(models.Product, {
        foreignKey: 'product_id',
        as: 'product'
      })
      ProductSku.belongsTo(models.OptionValue, {
        foreignKey: 'opt_val_id_1',
        as: 'variant1'
      })
      ProductSku.belongsTo(models.OptionValue, {
        foreignKey: 'opt_val_id_2',
        as: 'variant2'
      })
      ProductSku.belongsTo(models.OptionValue, {
        foreignKey: 'opt_val_id_3',
        as: 'variant3'
      })
    }
  };
  ProductSku.init({
    product_id: {
      type: DataTypes.INTEGER
    },
    sku: DataTypes.STRING,
    price: {
      type: DataTypes.NUMERIC,
      validate: { min: 1,  },
      allowNull: false
    },
    stock: {
      type: DataTypes.INTEGER,
      validate: { min: 0 },
      allowNull: false
    },
    opt_val_id_1: DataTypes.INTEGER,
    opt_val_id_2: DataTypes.INTEGER,
    opt_val_id_3: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'ProductSku',
    paranoid: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  });
  return ProductSku;
};