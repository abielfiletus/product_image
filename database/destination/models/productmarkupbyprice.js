'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductMarkupByPrice extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  ProductMarkupByPrice.init({
    lowerbound: DataTypes.INTEGER,
    upperbound: DataTypes.INTEGER,
    markup: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'ProductMarkupByPrice',
    paranoid: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  });
  return ProductMarkupByPrice;
};