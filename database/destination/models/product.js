'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Product.hasMany(models.ProductSku, {
        foreignKey: 'product_id',
        as: 'product_sku',
      });
      Product.belongsTo(models.Category1, {
        foreignKey: 'category1',
        as: 'category1_detail',
      });
      Product.belongsTo(models.Category2, {
        foreignKey: 'category2',
        as: 'category2_detail',
      });
      Product.belongsTo(models.Category3, {
        foreignKey: 'category3',
        as: 'category3_detail',
      });
      Product.hasMany(models.ProductSupplier, {
        foreignKey: 'product_id',
        as: 'product_supplier'
      });
      Product.hasMany(models.ProductReseller, {
        foreignKey: 'product_id',
        as: 'product_reseller'
      });
      Product.hasMany(models.ProductImage, {
        foreignKey: 'product_id',
        as: 'product_image'
      })
    }
  };
  Product.init({
    name: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    safe_url: DataTypes.TEXT,
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    weight: DataTypes.INTEGER,
    video_url: DataTypes.STRING,
    quality: DataTypes.STRING,
    QC_passed: DataTypes.BOOLEAN,
    status: DataTypes.STRING,
    category1: {
      type: DataTypes.STRING,
      allowNull: false
    },
    category2: DataTypes.STRING,
    category3: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Product',
    paranoid: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  });
  return Product;
};